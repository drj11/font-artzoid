# Evaluation of Counter Square LAB 20211027

From printed specimen.


## Response

- value of b d g m o p v y eth reduced
- tail of g (and y) redrawn
- widen s
- value (stroke width) of **f** **i** increased
- value of a slightly increased
- value of dieresis increased
- add ae abreve (and remaining latin breve letters)


## On value

The overall value does vary quite a lot across the line.
It's unusual, and may be distracting, but it doesn't appear to
harm readability.

Particularly dark: d o m g v y p (on left stroke) b

Particularly light: a f s

**g** has heavy bowl, light tail.

Icelandic and other non-English:
- eth is too dark
- ij unbalanced: i too light?
- dieresis very light (especially compared to i j dots)


## On spacing

narrowen: re op i-anything ss a(nrt) ff ke ov av (v on left)

Narrowen *space* (the word space).

Less space on apostrophe (both sides).
Maltese particularly good for this.
i'r (Welsh) looks fine on left of apostrophe, not so much on right.
kern i'?


## Other notes

Widen **s**.

*period*, perfect size.

*comma*, tail a little bit weak.

Missing:
- æ ortho liga
- (from Maltese), hbar, gdot, cdot, zdot
- abreve


# END
