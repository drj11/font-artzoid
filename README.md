# Artzoid

Based on a hand-and-mouse digitisation of a sketch from 2021-08-21.

An informal rough-cut font inspired by the likes of
[Ad Lib](https://fontsinuse.com/typefaces/7354/ad-lib) and
[Marujo](https://fontsinuse.com/typefaces/34959/marujo).


## Licensing

The font is intended to be distributed with a liberal licence
that allows most uses, but not distribution.
See LICENCE.md.

Also any use permitted for Welsh language and Icelandic language.


## Advance widths

The current alphabet in (advance) width order:

lijfrstxckqanuoþegpvzhdðybmw


## Awards

good letters: o b d z

## END
